var mongoose = require('mongoose');
var User = mongoose.model('User');
var ObjectId = mongoose.Types.ObjectId;

exports.addUser = function(req, res) {
  var userModel = new User(req.body);
  userModel.save(function(err, user) {
    if (err) {
      res.status(500);
      res.json({
        success: false,
        error: "Error occured: " + err
      })
    } else {
      res.json({
        success: true,
        user: user
      })
    }
  })
}

exports.loginUser = function(req, res) {
  var username = req.body.username;
  var password = req.body.password;

  User.findOne({ username: username, password: password })
    .exec(function(err, user) {
      if(err) {
        res.json({
          success: false,
          message: 'An error occured: ' + err
        })
      } else {
        if (user) {
          res.json({
            success: true,
            user: user
          })
        } else {
          res.json({
            success: false,
            message: 'Invalid username or password.'
          })
        }
      }
    });

}