var request = require('request');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var ObjectId = mongoose.Types.ObjectId;

exports.searchBooks = function(req, res) {
  var isbn = req.params['isbn']; //getting the isbn from url params
  //using the request module to send request to google book api and getting the result
  request.get('https://www.googleapis.com/books/v1/volumes?q=isbn:' + isbn, function(err, result) {
    if(err) {
      res.status(500);
      res.json({
        message: 'An error occured: ' + err
      });
    } else {
      var jsonResponse = JSON.parse(result.body);
      var books = jsonResponse.items;
      if(books) { //if the result from google is not empty, return the JSON result
        for(var i in books) {
          books[i] = books[i].volumeInfo;
        }
        res.json({
          books: books
        });
      } else { //if the result from google is empty, return this JSON
        res.json({
          message: 'No book found with your ISBN input'
        });
      }
    }
  });
}

exports.addToBooklist = function(req, res) {
  var userId = req.params['user_id'];
  var book = req.body;
  User.findById(new ObjectId(userId), function(err, user) {
      if(err) {
        res.status(500);
        res.json({
          success: false,
          message: 'An error occured: ' + err
        });
      } else {
        if (user) {
          user.books.push(book);
          user.save(function(err, user) {
            if (err) {
              res.status(500);
              res.json({
                success: false,
                error: "Error occured: " + err
              })
            } else {
              res.json({
                success: true,
                books: user.books
              })
            }
          })
        } else {
          res.json({
            success: false,
            message: 'Invalid user id.'
          })
        }
      }
    });
}

exports.getBookFromBooklist = function(req, res) {
  var userId = req.params['user_id'];
  var bookId = req.params['book_id'];
  User.findById(new ObjectId(userId), function(err, user) {
    if(err) {
      res.status(500);
      res.json({
        success: false,
        message: 'An error occured: ' + err
      });
    } else {
      if (user) {
        var books = user.books;
        for(var i in books) {
          if( books[i].id == bookId) {
            return res.json({
              success: true,
              book: books[i]
            })
          }
        }
        res.json({
          success: false,
          message: 'This book is not found on your booklist.'
        })
      } else {
        res.json({
          success: false,
          message: 'Cannot find user on the database.'
        })
      }
    }
  });
}

exports.deleteBookFromBooklist = function(req, res) {
  var userId = req.params['user_id'];
  var bookId = req.params['book_id'];
  User.findById(new ObjectId(userId), function(err, user) {
    if(err) {
      res.status(500);
      res.json({
        success: false,
        message: 'An error occured: ' + err
      });
    } else {
      if (user) {
        var books = user.books;
        var index;

        for(var i in books) {
          if( books[i].id == bookId) {
            index = i;
          }
        }
        if(index) {
          books.splice(index, 1);
          user.save(function(err, user) {
            if (err) {
              res.status(500);
              res.json({
                success: false,
                error: "Error occured: " + err
              })
            } else {
              res.json({
                success: true,
                books: books
              })
            }
          })
        } else {
          res.json({
            success: false,
            message: 'Cannot find the book on the booklist.'
          });
        }
      } else {
        res.json({
          success: false,
          message: 'Cannot find user on the database.'
        })
      }
    }
  });
}