/**
* Created with ISBNbooks.
* User: StayGrafting
* Date: 2015-06-28
* Time: 02:30 PM
* To change this template use Tools | Templates.
*/


var request = require('request');

exports.searchUserid = function(req, res, next){
    var us_id = req.query.id

    var url = 'https://staygrafting.iriscouch.com/isbn_users/'+us_id;
    
    request.get(url, function(err, response, body){
        body = JSON.parse(body);
        res.send(body);
        res.end();
    })
}


///put book into database
exports.addBook = function(req, res, next){
    var us_id = req.query.id
    var bookisbn = req.query.isbn

    var url = 'https://staygrafting.iriscouch.com/isbn_users/'+us_id;
    
    
    var checkPresence = function(obj, charSeq){
        var val = false;
        if (Array.isArray(obj)){
            
            
            for(var i =0; i < obj.length; i++ ){
                console.log(obj.books[i]+" and "+ charSeq);
                if(obj.books[i] == charSeq){
                    val = true;
                }
            }
            
            return val;
        }
        
    }
    
    request.get(url, function(err, response, body){
        if(response.statusCode == 404){
            
        }else if(response.statusCode == 200){
            var user_obj = JSON.parse(body);
            
            if(Array.isArray(user_obj.books)){
                 console.log(user_obj.books.length);
                
                if(checkPresence(user_obj.books, bookisbn)){
                    console.log('is there')
                    console.log(user_obj.books.length);
                    
                    res.send(user_obj);
                    res.end();
                }else{
                    console.log('not there')
                    console.log(user_obj.books.length);
                    user_obj.books.push(bookisbn);
                    res.send(user_obj);
                    res.end();
                }
                
                
            }
            
        }
    })
}






//put user into database
//
exports.addUser = function(req, res, next){
   
    var email = req.query.email;
    var fname = req.query.fname;
    var lname = req.query.lname;
    var pass = req.query.pass;
    
    var tstamp = Date.now();
    var bookarr = new Array();
    var usid = email
    var user = {
        id: usid,
        firstname: fname,
        lastname: lname,
        email: email,
        password: pass,
        books: bookarr
    };
    var url = 'https://staygrafting.iriscouch.com/isbn_users/'+usid;
    var strUser = JSON.stringify(user);

    var doc_params = {uri:url, body:strUser};
    console.log(user);
    
    request.get(url, function(err, response, body){
        if(response.statusCode == 404){
            console.log('try to put params');
            request.put(doc_params, function(err, response, body){
                res.send(user);
                res.end()
            });
        }else if(response.statusCode == 200){
            
        }
    })
}