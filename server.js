//Loads up all modules
var restify = require('restify');
var request = require('request');
var mongoose = require('mongoose');
/*
mongoose.connect('mongodb://localhost/bookapp');
var Schema = mongoose.Schema;

//Creates the schema
var UserSchema = new Schema({
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  books: {
    type: Array,
    default: []
  }
});
mongoose.model('User', UserSchema);


//Imports all controls for the API
var Books = require('./controllers/Book.js');
var Users = require('./controllers/User.js');
*/

var bookcon = require('./controller_modules/books.js');
var usercon = require('./controller_modules/user.js');
//Server creation using restify 
var server = restify.createServer();
server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.queryParser());

/*
//API routes
server.get("/books/search/:isbn", Books.searchBooks);
server.post("/users", Users.addUser);
server.post("/users/login", Users.loginUser);
server.post("/users/:user_id/books", Books.addToBooklist);
server.get("/users/:user_id/books/:book_id", Books.getBookFromBooklist);
server.del("/users/:user_id/books/:book_id", Books.deleteBookFromBooklist);

*/


server.listen(3000, function(){
    server.get("/books", bookcon.searchBookisbn);

    server.get("/user", usercon.searchUserid);
    
    
    server.put("/adduser", usercon.addUser);
    
    server.put("/addbook", usercon.addBook)
});
/*

//Starting the server on port 3000
var port = 3000;
server.listen(port, function (err) {
  if (err) {
    console.error(err);
  } else {
    console.log('API is ready at port: ' + port);
  }
});
*/