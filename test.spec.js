/**
* Created with ISBNbooks.
* User: StayGrafting
* Date: 2015-06-29
* Time: 04:52 PM
* To change this template use Tools | Templates.
*/


var frisby = require('frisby');




frisby.create('USER Route test')
    .get('http://telex-golf.codio.io:3000/user')
    .expectStatus(200).expectHeader('Content-Type', 'application/json').toss();

frisby.create('BOOK route test')
    .get('http://telex-golf.codio.io:3000/books')
    .expectStatus(200).expectHeader('Content-Type', 'application/json').toss();

