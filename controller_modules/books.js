/**
* Created with ISBNbooks.
* User: StayGrafting
* Date: 2015-06-28
* Time: 02:30 PM
* To change this template use Tools | Templates.
*/

var request = require('request');


exports.searchBookisbn = function(req, res, next){
    var bkisbn = req.query.isbn

    var googurl = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'+bkisbn
    
    request.get(googurl, function(err, response, body){
        body = JSON.parse(body);
        res.send(body);
        res.end();
    })
}